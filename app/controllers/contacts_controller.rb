class ContactsController < ApplicationController

    #controller for contact us page
    def contactus
    end

    def new
        @message=Contact.new
    end

    def create
        @message=Contact.create(message_params)
        if @message.save
            redirect_to root_path
        else
            render new_contact_path
        end
    end

    private
    def message_params
        params.require(:contact).permit(:title,:description,:email)
    end
end
